﻿using System.Collections.Generic;

namespace ILink.SubscriptionReminder
{
    public interface IListBuilder
    {
        IEnumerable<string> BuildList();
    }
}
