﻿using System;

namespace ILink.SubscriptionReminder.Properties
{
    public interface ISettings
    {
        string Message { get; }
        int[] PlansBeingNotified { get; }
    }

    public partial class Settings : ISettings { }
}
