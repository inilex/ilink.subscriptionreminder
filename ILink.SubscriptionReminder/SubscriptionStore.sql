﻿IF (EXISTS(SELECT * FROM [INFORMATION_SCHEMA].[TABLES] WHERE [TABLE_NAME] = 'LinkSubscriptionMap'))
    DROP TABLE [dbo].[LinkSubscriptionMap];

CREATE TABLE [dbo].[LinkSubscriptionMap]
(
	[PK_ID] INT NOT NULL IDENTITY(1,1),
	[FK_Account] INT NOT NULL,
	[FK_SubscriptionPlanID] INT NOT NULL,
	[FK_DeviceID] INT,
	[FK_VehicleID] INT,
	[PlanExpiration] DATETIME2 NOT NULL,
	[ReservedGeofencesStartIndex] INT,
	[LastNotified] DATETIME2
);