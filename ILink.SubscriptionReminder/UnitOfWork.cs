﻿namespace ILink.SubscriptionReminder
{
    using System.Data;

    public sealed class UnitOfWork : IUnitOfWork
    {
        private readonly IDbTransaction transaction;

        public UnitOfWork(IDbTransaction transaction)
        {
            this.transaction = transaction;
            this.transaction.Connection.Open();
        }

        public void Cancel()
        {
            this.transaction.Rollback();
        }

        public void Commit()
        {
            this.transaction.Commit();
        }

        public IDbCommand CreateCommand()
        {
            IDbCommand command = this.transaction.Connection.CreateCommand();
            command.Transaction = this.transaction;
            return command;
        }

        public void Dispose()
        {
            this.transaction.Dispose();
        }
    }
}
