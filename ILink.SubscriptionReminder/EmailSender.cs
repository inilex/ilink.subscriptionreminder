﻿using ILink.SubscriptionReminder.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;

namespace ILink.SubscriptionReminder
{
    public sealed class EmailSender : IEmailSender
    {
        private readonly SmtpClient client;
        private readonly ISettings settings;

        public EmailSender(ISettings settings, SmtpClient client)
        {
            this.client = client;
            this.settings = settings;
        }

        public void Send(IEnumerable<string> list, ExpirePeriod expirePeriod)
        {
            list.ToList().ForEach(this.Send);
        }

        private void Send(string email)
        {
            var message = new MailMessage();
            message.To.Add(email);
            message.Body = this.settings.Message;

            this.client.Send(message);
        }
    }
}
