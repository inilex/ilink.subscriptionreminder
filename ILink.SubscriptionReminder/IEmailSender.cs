﻿using System.Collections.Generic;

namespace ILink.SubscriptionReminder
{
    public interface IEmailSender
    {
        void Send(IEnumerable<string> list, ExpirePeriod expirePeriod);
    }
}
