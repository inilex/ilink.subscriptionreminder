﻿using ILink.SubscriptionReminder.Properties;
using System.Collections.Generic;

namespace ILink.SubscriptionReminder
{
    public sealed class ListBuilder : IListBuilder
    {
        private readonly ISettings settings;
        private readonly IUnitOfWork unitOfWork;

        public ListBuilder(ISettings settings, IUnitOfWork unitOfWork)
        {
            this.settings = settings;
            this.unitOfWork = unitOfWork;
        }

        public IEnumerable<string> BuildList()
        {
            return new List<string>();
        }
    }
}
