﻿namespace ILink.SubscriptionReminder
{
    public enum ExpirePeriod { Thirty = 30, Sixty = 60, Ninety = 90 };
}
