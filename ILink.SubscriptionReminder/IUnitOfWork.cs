﻿namespace ILink.SubscriptionReminder
{
    using System;
    using System.Data;

    public interface IUnitOfWork : IDisposable
    {
        void Cancel();

        void Commit();

        IDbCommand CreateCommand();
    }
}
