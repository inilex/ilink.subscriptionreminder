﻿namespace ILink.SubscriptionReminder.Tests
{
    using System;
    using System.Data;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public sealed class UnitOfWork_Fixture
    {
        private Mock<IDbConnection> connection;
        private Mock<IDbTransaction> transaction;
        private IUnitOfWork unitOfWork;

        [SetUp]
        public void SetUpTest()
        {
            this.connection = new Mock<IDbConnection>();
            this.transaction = new Mock<IDbTransaction>();

            this.transaction.Setup(t => t.Connection).Returns(this.connection.Object);

            this.unitOfWork = new UnitOfWork(this.transaction.Object);
        }

        [Test]
        public void Should_open_connection()
        {
            this.connection.Verify(c => c.Open());
        }

        [Test]
        public void Should_create_transactional_commands()
        {
            var expected = new Mock<IDbCommand>();
            this.connection.Setup(c => c.CreateCommand()).Returns(expected.Object);

            IDbCommand actual = this.unitOfWork.CreateCommand();

            Assert.AreSame(expected.Object, actual);
            expected.VerifySet(c => c.Transaction = It.Is<IDbTransaction>(t => t == this.transaction.Object));
        }

        [Test]
        public void Should_be_able_to_commit_transaction()
        {
            this.unitOfWork.Commit();
            this.transaction.Verify(t => t.Commit());
        }

        [Test]
        public void Should_be_able_to_cancel_transaction()
        {
            this.unitOfWork.Cancel();
            this.transaction.Verify(t => t.Rollback());
        }

        [Test]
        public void Should_be_disposable()
        {
            Assert.IsInstanceOf<IDisposable>(this.unitOfWork);
        }

        [Test]
        public void Should_dispose_transaction_on_dispose()
        {
            this.unitOfWork.Dispose();
            this.transaction.Verify(t => t.Dispose());
        }
    }
}
