﻿namespace ILink.SubscriptionReminder.Tests
{
    using ILink.SubscriptionReminder.Properties;
using Moq;
using NUnit.Framework;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;

    [TestFixture]
    public sealed class ListBuilder_Fixture
    {
        private Mock<ISettings> settings;
        private IListBuilder builder;
        private IDbConnection connection;

        [TestFixtureSetUp]
        public void SetUpFixture()
        {
            this.connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SubscriptionStore"].ConnectionString);
            this.connection.Open();
        }

        [TestFixtureTearDown]
        public void TearDownFixture()
        {
            this.connection.Close();
        }

        [SetUp]
        public void InitializeTest()
        {
            using (var reader = new StreamReader("SubscriptionStore.sql"))
            {
                var script = reader.ReadToEnd();

                using (var command = this.connection.CreateCommand())
                {
                    command.CommandText = script;
                    command.ExecuteNonQuery();
                }
            }

            this.settings = new Mock<ISettings>();
            var unitOfWork = new Mock<IUnitOfWork>();
            unitOfWork.Setup(u => u.CreateCommand()).Returns(() => this.connection.CreateCommand());
            this.builder = new ListBuilder(this.settings.Object, unitOfWork.Object);
        }

        [Test]
        public void Should_retrieve_user_emails_for_expiring_subscriptions()
        {
            // Arrange
            int i = CreateLinkSubMap(1234, 8, 2345654, 6787654, new DateTime(2015, 3, 14), 696969, new DateTime(2015, 1, 1));

            var actual = this.builder.BuildList();

            // Assert
            //   
        }

        private int CreateLinkSubMap(int account, int planId, int deviceId, int vehicleId, DateTime expiration, int geofenceStart, DateTime lastNotified)
        {
            IDbCommand command;
            IDbDataParameter parameter;
            int lsmID = 0;

            using (command = this.connection.CreateCommand())
            {
                command.CommandText = "INSERT INTO LinkSubscriptionMap (FK_Account, FK_SubscriptionPlanID, FK_DeviceID, FK_VehicleID, PlanExpiration, ReservedGeofencesStartIndex, LastNotified) " + 
                                      "VALUES (@Account, @PlanID, @DeviceID, @VehicleID, @Expiration, @GeofencesIndex, @LastNotified);SELECT SCOPE_IDENTITY()";
                parameter = command.CreateParameter();
                parameter.ParameterName = "@Account";
                parameter.Value = account;
                command.Parameters.Add(parameter);
                parameter = command.CreateParameter();
                parameter.ParameterName = "@PlanID";
                parameter.Value = planId;
                command.Parameters.Add(parameter);
                parameter = command.CreateParameter();
                parameter.ParameterName = "@DeviceID";
                parameter.Value = deviceId;
                command.Parameters.Add(parameter);
                parameter = command.CreateParameter();
                parameter.ParameterName = "@VehicleID";
                parameter.Value = vehicleId;
                command.Parameters.Add(parameter);
                parameter = command.CreateParameter();
                parameter.ParameterName = "@Expiration";
                parameter.Value = expiration;
                command.Parameters.Add(parameter);
                parameter = command.CreateParameter();
                parameter.ParameterName = "@GeofencesIndex";
                parameter.Value = geofenceStart;
                command.Parameters.Add(parameter);
                parameter = command.CreateParameter();
                parameter.ParameterName = "@LastNotified";
                parameter.Value = lastNotified;
                command.Parameters.Add(parameter);
                lsmID = Convert.ToInt32(command.ExecuteScalar());
            }

            return lsmID;
        }
    }
}
