﻿namespace ILink.SubscriptionReminder.Tests
{
    using ILink.SubscriptionReminder.Properties;
    using Moq;
    using nDumbster.Smtp;
    using NUnit.Framework;
    using System.Collections.Generic;
    using System.Net.Mail;
    using System.Linq;

    [TestFixture]
    public sealed class EmailSender_Fixture
    {
        private IEmailSender sender;
        private SimpleSmtpServer mailServer;
        private Mock<ISettings> settings;
        private SmtpClient mailClient;

        [SetUp]
        public void InitializeTest()
        {
            this.settings = new Mock<ISettings>();
            this.mailClient = new SmtpClient();
            this.sender = new EmailSender(this.settings.Object, this.mailClient);
            this.mailServer = SimpleSmtpServer.Start(50107);
        }

        [TearDown]
        public void FinalizeTest()
        {
            this.mailClient.Dispose();
            this.mailServer.Dispose();
        }

        [Test]
        public void Should_send_configured_email_to_list_of_addresses()
        {
            const string Message = "Expected email message";
            this.settings.SetupGet(s => s.Message).Returns(Message);
            var list = new HashSet<string>();
            list.Add("bogus2@inilex.com");
            list.Add("bogus1@inilex.com");

            this.sender.Send(list, ExpirePeriod.Thirty);

            IEnumerable<MailMessage> messages = this.mailServer.ReceivedEmail;
            Assert.AreEqual(Message, messages.Select(m => m.Body).Distinct().Single());
            CollectionAssert.AreEquivalent(list, messages.Select(m => m.To.Single().Address));
        }
    }
}
